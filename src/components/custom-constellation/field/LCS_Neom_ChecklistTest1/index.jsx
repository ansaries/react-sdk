import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import handleEvent from '@pega/react-sdk-components/lib/components/helpers/event-utils';
import Utils from '@pega/react-sdk-components/lib/components/helpers/utils';
import FieldValueList from '@pega/react-sdk-components/lib/components/designSystemExtension/FieldValueList';
import StyledLcsNeomChecklistTest1Wrapper from './styles';

function LcsNeomChecklistTest1(props) {
  const {
    getPConnect,
    label,
    value = [],
    datasource = [],
    validatemessage,
    readOnly,
    testId,
    displayMode,
    hideLabel,
    onRecordChange,
  } = props;

  const [options, setOptions] = useState([]);

  useEffect(() => {
    const list = Utils.getOptionList(props, getPConnect().getDataObject());
    setOptions(list);
  }, [datasource, getPConnect, props]);

  let readOnlyProp = {};

  if (readOnly) {
    readOnlyProp = { readOnly: true };
  }

  let testProp = {};
  testProp = {
    'data-test-id': testId
  };

  const handleCheckboxChange = (optionKey) => {
    const updatedValues = value.includes(optionKey)
      ? value.filter(v => v !== optionKey)
      : [...value, optionKey];

    handleEvent(getPConnect().getActionsApi(), 'changeNblur', getPConnect().getStateProps().value, updatedValues);
    if (onRecordChange) {
        onRecordChange(updatedValues);
    }
  };

  return options.length === 0 ? null : (
    <StyledLcsNeomChecklistTest1Wrapper>
      {options.map((option) => (
        <FormControlLabel
          key={option.key}
          control={
            <Checkbox
              checked={value.includes(option.key)}
              onChange={() => handleCheckboxChange(option.key)}
              {...readOnlyProp}
              {...testProp}
            />
          }
          label={getPConnect().getLocalizedValue(option.value)}
        />
      ))}
    </StyledLcsNeomChecklistTest1Wrapper>
  );
}

LcsNeomChecklistTest1.defaultProps = {
  value: [],
  validatemessage: '',
  displayAsStatus: false,
  datasource: [],
  hideLabel: false,
  readOnly: false,
  testId: null,
  displayMode: null,
};

LcsNeomChecklistTest1.propTypes = {
  value: PropTypes.arrayOf(PropTypes.string),
  displayMode: PropTypes.string,
  displayAsStatus: PropTypes.bool,
  label: PropTypes.string.isRequired,
  hideLabel: PropTypes.bool,
  getPConnect: PropTypes.func.isRequired,
  validatemessage: PropTypes.string,
  datasource: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.shape({
      source: PropTypes.arrayOf(PropTypes.object)
    })
  ]),
  readOnly: PropTypes.bool,
  testId: PropTypes.string,
};

export default LcsNeomChecklistTest1;
