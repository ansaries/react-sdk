import React from 'react';
import { KeyboardDateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DayjsUtils from '@date-io/dayjs';
import TextInput from '@pega/react-sdk-components/lib/components/field/TextInput';
import handleEvent from '@pega/react-sdk-components/lib/components/helpers/event-utils';
import FieldValueList from '@pega/react-sdk-components/lib/components/designSystemExtension/FieldValueList';
import PropTypes from 'prop-types';
import { format } from '@pega/react-sdk-components/lib/components/helpers/formatters/';
import { dateFormatInfoDefault, getDateFormatInfo} from '@pega/react-sdk-components/lib/components/helpers/date-format-utils';

import StyledLcsNeomCustomDateTimeWrapper from './styles';


// Duplicated runtime code from React SDK

// props passed in combination of props from property panel (config.json) and run time props from Constellation
// any default values in config.pros should be set in defaultProps at bottom of this file
export default function LcsNeomCustomDateTime(props: any) {
  const {
    getPConnect,
    label,
    required,
    disabled,
    value = '',
    validatemessage,
    status,
    onChange,
    readOnly,
    testId,
    helperText,
    displayMode,
    hideLabel
  } = props;

  const pConn = getPConnect();
  const actions = pConn.getActionsApi();
  const propName = pConn.getStateProps().value;
  const helperTextToDisplay = validatemessage || helperText;

  // Start with default dateFormatInfo
  const dateFormatInfo = dateFormatInfoDefault;
  // and then update, as needed, based on locale, etc.
  const theDateFormat = getDateFormatInfo()
  dateFormatInfo.dateFormatString = theDateFormat.dateFormatString;
  dateFormatInfo.dateFormatStringLC = theDateFormat.dateFormatStringLC;
  dateFormatInfo.dateFormatMask = theDateFormat.dateFormatMask;


  if (displayMode === 'LABELS_LEFT') {
    const formattedDateTime = format(props.value, 'datetime', { format: `${dateFormatInfo.dateFormatString} hh:mm a` });
    return <FieldValueList name={hideLabel ? '' : label} value={formattedDateTime} />;
  }

  if (displayMode === 'STACKED_LARGE_VAL') {
    const formattedDateTime = format(props.value, 'datetime', { format: `${dateFormatInfo.dateFormatString} hh:mm a` });
    return <FieldValueList name={hideLabel ? '' : label} value={formattedDateTime} variant='stacked' />;
  }

  if (readOnly) {
    const formattedDateTime = format(props.value, 'datetime');
    return <TextInput {...props} value={formattedDateTime} />;
  }

  const handleChange = date => {
    const changeValue = date && date.isValid() ? date.toISOString() : null;
    onChange({ value: changeValue });
  };

  const handleAccept = date => {
    const changeValue = date && date.isValid() ? date.toISOString() : null;
    handleEvent(actions, 'changeNblur', propName, changeValue);
  };

  //
  // TODO: Keyboard doesn't work in the minute field, it updates one digit then jump to am/pm field
  //       try an older version of the lib or use DateTimePicker
  //

  return (
    <StyledLcsNeomCustomDateTimeWrapper>
      <MuiPickersUtilsProvider utils={DayjsUtils}>
        <KeyboardDateTimePicker
          variant='inline'
          inputVariant='outlined'
          fullWidth
          autoOk
          required={required}
          disabled={disabled}
          placeholder={`${dateFormatInfo.dateFormatStringLC} hh:mm a`}
          format={`${dateFormatInfo.dateFormatString} hh:mm a`}
          mask={`${dateFormatInfo.dateFormatMask} __:__ _m`}
          minutesStep={5}
          error={status === 'error'}
          helperText={helperTextToDisplay}
          size='small'
          label={label}
          value={value || null}
          onChange={handleChange}
          onAccept={handleAccept}
          data-test-id={testId}
        />
      </MuiPickersUtilsProvider>
    </StyledLcsNeomCustomDateTimeWrapper>
  );
}

LcsNeomCustomDateTime.defaultProps = {
  value: undefined,
  validatemessage: "",
  helperText: "",
  hideLabel: false,
  disabled: false,
  readOnly: false,
  required: false,
  testId: null,
  displayMode: null,
  variant: "inline"
};

LcsNeomCustomDateTime.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string.isRequired,
  hideLabel: PropTypes.bool,
  getPConnect: PropTypes.func.isRequired,
  validatemessage: PropTypes.string,
  helperText: PropTypes.string,
  disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  readOnly: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  testId: PropTypes.string,
  displayMode: PropTypes.string,
  variant: PropTypes.string
};
